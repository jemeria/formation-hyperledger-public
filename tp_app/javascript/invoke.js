/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

async function main() {
    try {
        // load the network configuration
        // TODO

        // Create a new file system based wallet for managing identities.
        // TODO

        // Check to see if we've already enrolled the user.
        // TODO

        // Create a new gateway for connecting to our peer node.
        // TODO

        // Get the network (channel) our contract is deployed to.
        // TODO

        // Get the contract from the network.
        // TODO

        // Submit the specified transaction.
        // createCar transaction - requires 5 argument, ex: ('createCar', 'CAR12', 'Honda', 'Accord', 'Black', 'Tom')
        // changeCarOwner transaction - requires 2 args , ex: ('changeCarOwner', 'CAR10', 'Dave')
        // TODO

        // Disconnect from the gateway.
        // TODO

    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        process.exit(1);
    }
}

main();
