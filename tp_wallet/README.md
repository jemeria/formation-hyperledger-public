# Wallet

Gestion des identités pour une application

Utilisation du Certificate Authority et du connection profile

## Etude du connection profile

Rechercher les fichiers contenant les profiles de connexion et les voir ce qu'il contienne

```bash
cd ~/fabric-samples/test-network
find . -name 'connection*'
```

## Si ce n'est pas fait démarrer le chaincode Fabcar

```bash
./network.sh deployCC
```

## Développer des scripts d'alimentation du Wallet

Suite à l'exécution des commandes suivantes, l'admin et l'utilisateur ```user1``` doivent être présent dans le Wallet. Pour cela compléter les codes.
Documentation : https://hyperledger.github.io/fabric-sdk-node/release-1.4/module-fabric-network.Wallet.html
https://hyperledger.github.io/fabric-sdk-node/release-1.4/module-fabric-network.Gateway.html
https://hyperledger.github.io/fabric-sdk-node/release-1.4/CertificateAuthority.html

```bash
cd ~/formation-hyperledger-public/tp_wallet
npm install
node enrollAdmin.js
node registerUser.js
```

Résultat attendu :
```bash
juziit@fhlf:~/formation-hyperledger-public/tp_wallet$ node enrollAdmin.js
Wallet path: /home/juziit/formation-hyperledger-public/tp_wallet/wallet
Successfully enrolled admin user "admin" and imported it into the wallet
juziit@fhlf:~/formation-hyperledger-public/tp_wallet$ node registerUser.js
Wallet path: /home/juziit/formation-hyperledger-public/tp_wallet/wallet
Successfully registered and enrolled admin user user_alyra and imported it into the wallet
juziit@fhlf:~/formation-hyperledger-public/tp_wallet$ cd wallet/
juziit@fhlf:~/formation-hyperledger-public/tp_wallet/wallet$ ls
admin  user_alyra
juziit@fhlf:~/formation-hyperledger-public/tp_wallet/wallet$ cd user_alyra/
juziit@fhlf:~/formation-hyperledger-public/tp_wallet/wallet/user_alyra$ ls
878a0e7ed2ec15c3bb0d80b167ee95981088f716b5f719de7e756ac9c8466610-priv  user_alyra
878a0e7ed2ec15c3bb0d80b167ee95981088f716b5f719de7e756ac9c8466610-pub
juziit@fhlf:~/formation-hyperledger-public/tp_wallet/wallet/user_alyra$ cat user_alyra
{"name":"user_alyra","mspid":"Org1MSP","roles":null,"affiliation":"","enrollmentSecret":"","enrollment":{"signingIdentity":"878a0e7ed2ec15c3bb0d80b167ee95981088f716b5f719de7e756ac9c8466610","identity":{"certificate":"-----BEGIN CERTIFICATE-----\nMIICWDCCAf+gAwIBAgIUMwq/YSn5pHEfzEP1K+k1oU25V+IwCgYIKoZIzj0EAwIw\ncDELMAkGA1UEBhMCVVMxFzAVBgNVBAgTDk5vcnRoIENhcm9saW5hMQ8wDQYDVQQH\nEwZEdXJoYW0xGTAXBgNVBAoTEG9yZzEuZXhhbXBsZS5jb20xHDAaBgNVBAMTE2Nh\nLm9yZzEuZXhhbXBsZS5jb20wHhcNMjAwMzE4MDM1MzAwWhcNMjEwMzE4MDM1ODAw\nWjAmMQ8wDQYDVQQLEwZjbGllbnQxEzARBgNVBAMMCnVzZXJfYWx5cmEwWTATBgcq\nhkjOPQIBBggqhkjOPQMBBwNCAASuoWsIVmBsN6J2WOfcshGB2yYXn+0ELC1rfxuS\nDWTdfB8rB0xW1jLeECizmTWrbs9ynpu9pP5vbsEkDCuGeYUgo4HAMIG9MA4GA1Ud\nDwEB/wQEAwIHgDAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBSI2/P0Bj4PAeflItcy\ngEfrj5rSpjAfBgNVHSMEGDAWgBRop32VfAI7y1BiV5G/G/A+GIm2cDBdBggqAwQF\nBgcIAQRReyJhdHRycyI6eyJoZi5BZmZpbGlhdGlvbiI6IiIsImhmLkVucm9sbG1l\nbnRJRCI6InVzZXJfYWx5cmEiLCJoZi5UeXBlIjoiY2xpZW50In19MAoGCCqGSM49\nBAMCA0cAMEQCIDpMZuAdpsvAI6OlWoa1o84uUMB3ChnOcuI1+yth2esIAiAkZrDn\nleZOYeoI036TkdbuHUIoExFed6ReJ1NwSOeypg==\n-----END CERTIFICATE-----\n"}}}
```
