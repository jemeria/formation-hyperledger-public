'use strict';

const express = require('express');
const bodyParser = require('body-parser');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.use(bodyParser.json());

// Setting for Hyperledger Fabric
const { FileSystemWallet, Gateway } = require('fabric-network');
const path = require('path');
// TODO

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.get('/api/fabcar/query_car/:car_number', async function (req, res) {
// TODO
});

app.post('/api/fabcar/create_car', async function (req, res) {
// TODO
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

