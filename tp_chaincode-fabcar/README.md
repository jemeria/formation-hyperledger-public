# TP : Développement de function dans un smartcontract

## Objectif

Etre capable de résaliser des fonctions de lecture/écriture sur une blockchain Hyperledger.

## Mise en place

Pour ce TP, nous allons utiliser le test-network précédement installé. Pour créer l'environement de TP, il faudra modifier le chaincode existant. Pour cela, exécuter le commande ci-dessous en partant du répertoire contenant ce README.

```bash
mv ~/fabric-samples/chaincode/fabcar/javascript ~/fabric-samples/chaincode/fabcar/javascript.solution
cp -r chaincode ~/fabric-samples/chaincode/fabcar/javascript
```
## Travail demandé

Compléter les annotations TODO présentent dans le script ```~/fabric-samples/chaincode/fabcar/javascript/lib/fabcar.js```

## Informations complémentaires

### Publication du chaincode

Pour publier le chaincode mise à jour, il faut préciser que c'est le chaincode javascript que vous souhaitez installer et incrémenter le numéro de version à chaque exécution. Exemple :
```bash
cd ~/fabric-samples/test-network
./network.sh deployCC -l javascript -v 3
```

### Variable d'environnement
Pour utiliser la commande ```peer```, il faut positionner lesvariables d'environnement suivantes :
```bash
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
```

## Résultat attendu

```bash
$ peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ~/fabric-samples/test-network/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n fabcar --peerAddresses localhost:7051 --tlsRootCertFiles ~/fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ~/fabric-samples/test-network/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"createCar","Args":["CAR99999", "VENTURI","VANTAGE","BLUE", "Jeremie"]}'
2020-03-12 15:32:28.178 UTC [chaincodeCmd] chaincodeInvokeOrQuery -> INFO 001 Chaincode invoke successful. result: status:200

$ peer chaincode query -C mychannel -n fabcar -c '{"Args":["queryCar","CAR99999"]}'
{"color":"BLUE","make":"VENTURI","model":"VANTAGE","owner":"Jeremie","docType":"car"}
```

