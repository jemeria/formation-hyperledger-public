# TP private data

## Préparation du réseau

Pour les collections, il est nécessaire d'utiliser la base couchdb pour le Whole state.
Par défaut, levedb n'est pas compatible.

```bash
cd ~/fabric-samples/test-network
./network.sh up createChannel -s couchdb -ca
```

## Découverte du code à remplir

Recopier le code à trou sur le fabric sample pour développer le code
```bash
cp -r ~/formation-hyperledger-public/tp_private_data-fabcar/* ~/fabric-samples/chaincode/fabcar/
```

Etudier la configuration des collections et en déduire qui pourra voir les données privées.

## Développer la solution

Compléter le fichier ```~/fabric-samples/chaincode/fabcar/javascript/lib/fabcar.js``` de façon à ajouter le prix de vente des voitures en données privées et ajouter une méthode ```readTxPrice``` pour récupérer cette valeur

## Déploiement du chaincode

Positionnement des variables représentant Org1 :

```bash
cd ~/fabric-samples/test-network
export PATH=${PWD}/../bin:${PWD}:$PATH
export FABRIC_CFG_PATH=$PWD/../config/
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
```

Packaging

```bash
peer lifecycle chaincode package fabcar.tar.gz --path ~/fabric-samples/chaincode/fabcar/javascript/ --lang node --label fabcarpv1
```

Installation sur org1
```bash
peer lifecycle chaincode install fabcar.tar.gz
```

Installation sur org2
```bash
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
export CORE_PEER_ADDRESS=localhost:9051
peer lifecycle chaincode install fabcar.tar.gz
```

Approbation du chaincode et de la configuration des données privées par org1
```bash
export CC_PACKAGE_ID=$(peer lifecycle chaincode queryinstalled | sed -n "/fabcarpv1/{s/^Package ID: //; s/, Label:.*$//; p;}")
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name fabcar --version 1.0 --collections-config ../chaincode/fabcar/collections_config.json --signature-policy "OR('Org1MSP.member','Org2MSP.member')" --init-required --package-id $CC_PACKAGE_ID --sequence 1 --tls true --cafile $ORDERER_CA
```

Approbation du chaincode et de la configuration des données privées par org2
```bash
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
export CORE_PEER_ADDRESS=localhost:9051
peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name fabcar --version 1.0 --collections-config ../chaincode/fabcar/collections_config.json --signature-policy "OR('Org1MSP.member','Org2MSP.member')" --init-required --package-id $CC_PACKAGE_ID --sequence 1 --tls true --cafile $ORDERER_CA
```

Commit du chaincode
```bash
export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
export ORG1_CA=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export ORG2_CA=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name fabcar --version 1.0 --sequence 1 --collections-config ../chaincode/fabcar/collections_config.json --signature-policy "OR('Org1MSP.member','Org2MSP.member')" --init-required --tls true --cafile $ORDERER_CA --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_CA --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_CA
```

Initialise la liste des voitures
```bash
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile $ORDERER_CA -C mychannel -n fabcar --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_CA --peerAddresses localhost:9051 --tlsRootCertFiles $ORG2_CA --isInit -c '{"function":"initLedger","Args":[]}'
```

## Que se passe-t-il lorsque l'on demande le prix de la voiture CAR0?

```bash
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
peer chaincode query -C mychannel -n fabcar -c '{"Args":["queryCar","CAR0"]}'
peer chaincode query -C mychannel -n fabcar -c '{"Args":["readTxPrice","CAR0"]}'
```

## Ajout de données privées

Réaliser une vente de la voiture CAR0

```bash
peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name fabcar --tls true --cafile $ORDERER_CA --peerAddresses localhost:7051 --tlsRootCertFiles $ORG1_CA -c '{"Args":["changeCarOwner", "CAR0", "Jérémie", "100000"]}'
```

##  Vérifier que le prix est bien visible uniquement par l'ORG1

Pour Org1 
```bash
peer chaincode query -C mychannel -n fabcar -c '{"Args":["readTxPrice","CAR0"]}'
```

Pour Org2MSP
```bash
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
export CORE_PEER_ADDRESS=localhost:9051
peer chaincode query -C mychannel -n fabcar -c '{"Args":["readTxPrice","CAR0"]}'
```

